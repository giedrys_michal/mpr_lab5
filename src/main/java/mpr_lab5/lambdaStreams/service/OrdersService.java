package mpr_lab5.lambdaStreams.service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import mpr_lab5.lambdaStreams.model.ClientDetails;
import mpr_lab5.lambdaStreams.model.Order;

public class OrdersService {

    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
    	orders.stream()
    			.filter(order -> order.getItems().size() > 5);    	
    	
    	return orders;
    }

    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
    	ClientDetails oldestClient = orders.stream()
    			.map(order -> order.getClientDetails())
    			.max(Comparator.comparing(ClientDetails::getAge))
    			.get();
    	
    	return oldestClient;
    }

    public static Order findOrderWithLongestComments(List<Order> orders) {
    	Order orderWithLongestComment = orders.stream()
    			.max(Comparator.comparing(Order::getComments))
    			.get();
    	
		return orderWithLongestComment;
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
    	String commaString = "";
    	/*
    	commaString += orders.stream()
    			.map(order -> order.getClientDetails())
    			.filter(client -> client.getAge() > 18)
    			.map(i -> {
    				String name = i.getName() + ",";
    				String surname = i.getSurname() + ",";
    				final String tmp = name + surname;
    				return tmp;
    			});
    	
    	return commaString.replaceAll(",$", "");
    	*/
    }

    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
    	/*
    	List<String> orderNames = orders.stream()
    			.map(order -> {
    				order.getComments();
    				order.getItems();
    			})
    	*/
    			
    }

    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
    	orders.stream()
    			.map(order -> order.getClientDetails())
    			.filter(client -> client.getLogin().toUpperCase().matches("^S.*"))
    			.forEach(System.out::println);
    }

    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
    	Map<ClientDetails, List<Order>> groupedByClient = orders.stream()
    			.collect(Collectors.groupingBy(Order::getClientDetails));
    	
    	return groupedByClient;
    }

    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
        Map<Boolean, List<ClientDetails>> partition = orders.stream()
        		.map(order -> order.getClientDetails())
        		.collect(Collectors.partitioningBy(client -> client.getAge() > 18));
        
        return partition;
    }

}
